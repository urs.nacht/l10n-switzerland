*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

======================
Swiss import v11 files
======================

This module will adds functionality to import v11 files

Usage
=====

To import v11, use the wizard provided in bank statement.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Nicolas Bessi <nicolas.bessi@camptocamp.com>
* Vincent Renaville <vincent.renaville@camptocamp.com>
* Yannick Vaucher <yannick.vaucher@camptocamp.com>
* Romain Deheele <romain.deheele@camptocamp.com>
* Thomas Winteler <info@win-soft.com>
* Joël Grand-Guillaume <joel.grandguillaume@camptocamp.com>
* Guewen Baconnier <guewen.baconnier@camptocamp.com>
* Alex Comba <alex.comba@agilebg.com>
* Lorenzo Battistini <lorenzo.battistini@agilebg.com>
* Paul Catinean <paulcatinean@gmail.com>
* Paulius Sladkevičius <paulius@hbee.eu>
* David Coninckx <dco@open-net.ch>
* Akim Juillerat <akim.juillerat@camptocamp.com>
* Mykhailo Panarin <m.panarin@mobilunity.com>