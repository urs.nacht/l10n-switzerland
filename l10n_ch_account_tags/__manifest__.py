# Copyright 2018 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    'name': 'Switzerland Account Tags',
    'category': 'Localisation',
    'summary': '',
    'version': '1.0.1.0.0',
    'author': 'Camptocamp SA, Flectra Community, Odoo Community Association (OCA)',
    'website': 'https://gitlab.com/flectra-community/l10n-switzerland',
    'license': 'AGPL-3',
    'depends': [
        'l10n_ch',
    ],
    'data': [
        'data/account_account_tag.xml',
        'data/account_account_template.xml',
    ],
}
