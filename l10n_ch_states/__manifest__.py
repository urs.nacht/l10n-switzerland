# Copyright 2015 Mathias Neef copadoMEDIA UG
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    'name': 'Switzerland Country States',
    'category': 'Localisation',
    'summary': '',
    'version': '1.0.1.0.0',
    'author': 'copado MEDIA UG, Flectra Community, Odoo Community Association (OCA)',
    'website': 'https://gitlab.com/flectra-community/l10n-switzerland',
    'license': 'AGPL-3',
    'depends': [
        'base',
    ],
    'data': ['data/res_country_states.xml'],
    'demo': [],
    'installable': True,
    'application': False,
}
