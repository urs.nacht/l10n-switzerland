*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

==========================
Switzerland Country States
==========================

This module extends res.country.states for Switzerland. It brings states with
code and name for those who only want the states without cities.

For those who also want cities, please install *Switzerland - Postal codes (ZIP) list*  (`l10n_ch_zip`).


Installation
============

To install this module, you need to:

* download and install manually
* or directly install it over Odoo-Apps


Usage
=====

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Mathias Neef <mn@copado.de>