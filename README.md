# Flectra Community / l10n-switzerland

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_ch_zip](l10n_ch_zip/) | 1.0.1.0.0| Provides all Swiss postal codes for auto-completion
[l10n_ch_bank](l10n_ch_bank/) | 1.0.1.0.2| Banks names, addresses and BIC codes
[l10n_ch_base_bank](l10n_ch_base_bank/) | 1.0.1.1.0| Types and number validation for swiss electronic pmnt. DTA, ESR
[l10n_ch_account_tags](l10n_ch_account_tags/) | 1.0.1.0.0| Switzerland Account Tags
[l10n_ch_states](l10n_ch_states/) | 1.0.1.0.0| Switzerland Country States
[l10n_ch_import_isr_v11](l10n_ch_import_isr_v11/) | 1.0.1.0.0| Import of the ISR v11 files
[l10n_ch_mis_reports](l10n_ch_mis_reports/) | 1.0.1.1.0| Specific MIS reports for switzerland localization
[l10n_ch_dta](l10n_ch_dta/) | 1.0.1.0.1| Electronic payment file for Swiss bank (DTA)
[l10n_ch_payment_slip](l10n_ch_payment_slip/) | 1.0.1.0.0| Print inpayment slip from your invoices
[l10n_ch_pain_base](l10n_ch_pain_base/) | 1.0.1.0.0| ISO 20022 base module for Switzerland
[l10n_ch_pain_credit_transfer](l10n_ch_pain_credit_transfer/) | 1.0.1.0.0| Generate ISO 20022 credit transfert (SEPA and not SEPA)


