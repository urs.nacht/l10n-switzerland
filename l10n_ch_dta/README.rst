*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

==========================
Switzerland DTA
==========================


This module implements DTA. Note that it depends on these two modules:
'l10n_ch_base_bank' and 'account_payment_order'




Installation
============

To install this module, you need to:

* download and install manually
* or directly install it over Odoo-Apps


Usage
=====

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>* Nicolas Bessi (Camptocamp)
* Vincent Renaville vincent.renaville@camptocamp.com
* Joël Grand-Guillaume joel.grandguillaume@camptocamp.com
* Guewen Baconnier guewen.baconnier@camptocamp.com
* Paul Catinean paulcatinean@gmail.com
* Nicola Malcontenti nicola.malcontenti@agilebg.com
* Alex Comba alex.comba@agilebg.com
* Miguel Tallón <miguel.tallon@braintec-group.com>
* Kumar Aberer <kumar.aberer@braintec-group.com>

Funders
-------
* Hasa SA
* Open Net SA
* Prisme Solutions Informatique SA
* Quod SA