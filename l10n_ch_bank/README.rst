*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

===============
Swiss bank list
===============

This module will load all the Swiss banks with their name, address and
BIC code to ease the input of bank accounts.

It is not mandatory to use them in Odoo in Switzerland, but can improve
the user experience.

.. important:: The module contains the newest bank data (21.10.2014).
   If you want to update all your banks, update them via the link
   'Update Banks' in the section 'Bank & Cash' under
   ``Settings/Configuration/Invoicing``.

Usage
=====

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Nicolas Bessi <nicolas.bessi@camptocamp.com>
* Olivier Jossen (brain-tec AG)
* Guewen Baconnier <guewen.baconnier@camptocamp.com>
* Yannick Vaucher <yannick.vaucher@camptocamp.com>