# Copyright 2012-2017 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{'name': 'Switzerland - Bank type',
 'summary': 'Types and number validation for swiss electronic pmnt. DTA, ESR',
 'version': '1.0.1.1.0',
 'author': "Camptocamp,Flectra Community, Odoo Community Association (OCA)",
 'category': 'Localization',
 'website': 'https://gitlab.com/flectra-community/l10n-switzerland',
 'license': 'AGPL-3',
 'depends': ['account_payment_partner', 'base_iban'],
 'data': [
     'views/bank.xml',
     'views/invoice.xml',
 ],
 'demo': [],
 'test': [],
 'auto_install': False,
 'installable': True,
 'images': []
 }
