*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=======================================
Switzerland - ISO 20022 credit transfer
=======================================

This module adds support for *pain.001.001.03.ch.02* which is used for ISO 20022 credit transfers in Switzerland (SEPA or not SEPA).

It implements the guidelines for `ISO 20022 credit transfers <http://www.six-interbank-clearing.com/dam/downloads/fr/standardization/iso/swiss-recommendations/implementation_guidelines_ct.pdf>`_ published by SIX Interbank Clearing.

Configuration
=============

In the menu *Accounting > Configuration > Management > Payment Methods*,
select the payment method that has the code *sepa_credit_transfer* and
set the *PAIN Version* to *pain.001.001.03.ch.02 (credit transfer in Switzerland)*.

Usage
=====

On the payment order, you will see a new computed boolean field named
*ISR* which shows if the payment order is BVR or not.

This module doesn't modify the standard usage of the modules
*account_payment_order* and *account_banking_sepa_credit_transfer*.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Alexis de Lattre <alexis.delattre@akretion.com>
* Denis Leemann <denis.leemann@camptocamp.com>
* Mykhailo Panarin <m.panarin@mobilunity.com>