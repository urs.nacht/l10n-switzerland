*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

===================================
Switzerland - ISO 20022 base module
===================================

This module doesn't do anything by himself. It is the base module for 2 other modules:

* the module *l10n_ch_pain_credit_transfer* which adds support for the file format *pain.001.001.03.ch.02* which is used for ISO 20022 credit transfers (SEPA or not SEPA),

* the module *l10n_ch_pain_direct_debit* which adds support for the file format *pain.008.001.02.ch.01* which is used for ISO 20022 direct debits.

Usage
=====

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Alexis de Lattre <alexis.delattre@akretion.com>
* Denis Leemann <denis.leemann@camptocamp.com>
* Mykhailo Panarin <m.panarin@mobilunity.com>