*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3


============================
Swiss postal code (ZIP) list
============================

This module will load all the Swiss postal codes (ZIP) to ease the input
of partners.

It is not mandatory to use them in Odoo in Switzerland, but can improve
the user experience.


Installation
============

To install this module, you need to:

* download and install manually
* or directly install it over Odoo-Apps


Dependencies
============

The module ``base_location`` is required. It is available in
https://github.com/OCA/partner-contact/

Since Version 8.0.2.0 the module ``l10n_ch_states`` is required.
It is also available in https://github.com/OCA/l10n-switzerland


Configuration
=============

To configure this module, you need to:

* do nothing


Usage
=====

To use this module, you need to:

* Fill the new field in the partner form with a zip or a city from Switzerland
* Than you get a list with possible entries.
* The one you select is auto-filled in the zip-, citiy-, state- and country-field.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Nicolas Bessi (Camptocamp)
* Olivier Jossen (Brain Tec)
* Guewen Baconnier (Camptocamp)
* Mathias Neef (copadoMEDIA)
* Yannick Vaucher (Camptocamp)